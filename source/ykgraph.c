/*   ykgraph.c                                    F. Vernotte - 2015/06/24  */
/*   Plot the normalized frequency deviation samples of a .ykt file versus  */
/*   time                                                                   */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systems and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                              Modification for decimation : FV 2023/06/13 */
                                    
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include "sigma_theta.h"

#define db(x) ((double)(x))
#define sisig(x) ( (x) == 0 ) ? (db(0)) : (  ( (x) > 0 ) ? (db(1)) : (db(-1))  )

void usage(void)
/* Help message */
    {
    printf("Usage: YkGraph yktFILE\n\n");
    printf("Plots the graph of the normalized frequency deviation samples versus time.\n\n");
    printf("The input file yktFILE contains a 2-column table with dates (in s) in the first column and frequency deviation samples (yk) in the second column.\n\n");
    printf("The file yktFILE.gnu is generated for invoking gnuplot.\n");
    printf("The file yktFILE.pdf is the pdf file of the gnuplot graph (if the PDF option has been chosen in the configuration file).\n\n");
    printf("If the option '-d' is selected, the number of data is decimated to be limited to 10 000 samples\n\n");
    printf("SigmaTheta %s %s - FEMTO-ST/OSU THETA/Universite de Franche-Comte/CNRS - FRANCE\n",st_version,st_date);
    }

int main(int argc, char *argv[])
    {
    char source[256], dsource[256];
    int err,i,j,dcoef, N, dN;
    double tot_dur,ksy;
    char fv, idec, rep;
    char scalex=0, scaley=0; 
    double dumT[12000], dumY[12000];
    FILE *ofd;

    fv=idec=0;
    while ((rep = getopt (argc, argv, "gcmfdi")) != -1)
	switch(rep)
			{
			case 'd':
				idec = 1;
				break;
			case '?':
				printf("# Unknown option '-%c'\n",optopt);
				usage();
			default:
				exit(-1);
			}
     if (argc-optind<1)
		{
		printf("# Missing arguments\n");
		usage();
		exit(-1);
		}
    if (argc-optind>1)
		{
		printf("# Too many arguments\n");
		usage();
		exit(-1);
		}
    strcpy(source,argv[optind]);

    err=init_flag();
    flag_variance=fv;
    N=load_ykt(source,scalex,scaley,0);
    if (N==-1)
      printf("# File %s not found\n",source);
    else
        {
        if (N<2)
	    {
	    printf("# %s: unrecognized file\n\n", source);
	    usage();
	    }
        else
	    {
	    if ((idec)&&(N>10000))
	    	{
	    	dcoef=N/10000;
	    	dN=N/dcoef;
     		strcpy(dsource,source);
    		strcat(dsource,"-dcm");    		
		ofd=fopen(dsource, "w");
	    	for(j=0;j<dN;++j)
	    		{
	    		i=j*dcoef;
	    		dumT[j]=T[i];
	    		dumY[j]=Y[i];
	    		fprintf(ofd,"%.4e %.4e\n",dumT[j],dumY[j]);
	    		}
	    	err=gen_linplt(dsource,dN,dumT,dumY,1);
	    	if (err) printf("# Error %d: pdf file not created\n",err);
	    	}
	    else
	    	{
/* Use of gnuplot for generating the graph as a pdf file */
	    	err=gen_linplt(source,N,T,Y,1);
	    	if (err) printf("# Error %d: pdf file not created\n",err);
	    	}
	    }
	}
    }
		  
